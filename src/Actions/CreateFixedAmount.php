<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Actions;

use Bittacora\Bpanel4\Shipping\FixedAmount\Dtos\CreateFixedAmountDto;
use Bittacora\Bpanel4\Shipping\FixedAmount\Validation\FixedAmountValidator;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount;
use Illuminate\Validation\ValidationException;

final class CreateFixedAmount
{
    public function __construct(private readonly FixedAmountValidator $validator)
    {
    }

    /**
     * @throws ValidationException
     */
    public function handle(CreateFixedAmountDto $dto): void
    {
        $this->validator->validateFixedAmountCreation((array) $dto);

        $fixedAmount = new FixedAmount();
        $fixedAmount->setShippingZoneId($dto->shipping_zone_id);
        $fixedAmount->setName($dto->name);
        $fixedAmount->setActive($dto->active);
        $fixedAmount->setFixedAmount($dto->fixed_amount);
        $fixedAmount->setCartAmountPercentage($dto->cart_amount_percentaje);
        $fixedAmount->setQuantityMultiplier($dto->quantity_multiplier);
        $fixedAmount->setMinimum($dto->minimum);
        $fixedAmount->setMaximum($dto->maximum);

        $fixedAmount->save();
    }
}
