<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Actions;

use Bittacora\Bpanel4\Shipping\FixedAmount\Dtos\UpdateFixedAmountDto;
use Bittacora\Bpanel4\Shipping\FixedAmount\Validation\FixedAmountValidator;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount;

final class UpdateFixedAmount
{
    public function __construct(private readonly FixedAmountValidator $validator)
    {
    }

    public function handle(UpdateFixedAmountDto $dto): void
    {
        $this->validator->validateFixedAmountUpdate((array) $dto);

        $fixedAmount = FixedAmount::whereId($dto->id)->firstOrFail();

        $fixedAmount->setShippingZoneId($dto->shipping_zone_id);
        $fixedAmount->setName($dto->name);
        $fixedAmount->setActive($dto->active);
        $fixedAmount->setFixedAmount($dto->fixed_amount);
        $fixedAmount->setCartAmountPercentage($dto->cart_amount_percentaje);
        $fixedAmount->setQuantityMultiplier($dto->quantity_multiplier);
        $fixedAmount->setMinimum($dto->minimum);
        $fixedAmount->setMaximum($dto->maximum);

        $fixedAmount->save();
    }
}
