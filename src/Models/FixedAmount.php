<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Models\ShippingMethods;

use Bittacora\Bpanel4\Prices\Casts\PriceCast;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Traits\HasShippingClasses;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount
 *
 * @property int $id
 * @property int $shipping_zone_id
 * @property string $name
 * @property Price|null $fixed_amount
 * @property float|null $quantity_multiplier
 * @property float|null $cart_amount_percentage
 * @property bool $active
 * @property Price|null $minimum
 * @property Price|null $maximum
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|FixedAmount newModelQuery()
 * @method static Builder|FixedAmount newQuery()
 * @method static Builder|FixedAmount query()
 * @method static Builder|FixedAmount whereCartAmountPercentage($value)
 * @method static Builder|FixedAmount whereCreatedAt($value)
 * @method static Builder|FixedAmount whereFixedAmount($value)
 * @method static Builder|FixedAmount whereId($value)
 * @method static Builder|FixedAmount whereMaximum($value)
 * @method static Builder|FixedAmount whereMinimum($value)
 * @method static Builder|FixedAmount whereName($value)
 * @method static Builder|FixedAmount whereQuantityMultiplier($value)
 * @method static Builder|FixedAmount whereShippingZoneId($value)
 * @method static Builder|FixedAmount whereUpdatedAt($value)
 * @mixin Eloquent
 */
final class FixedAmount extends Model implements ShippingMethod
{
    use HasShippingClasses;
    
    /** @var string $table */
    public $table = 'shipping_methods_fixed_amount';

    /**
     * @var array<string, string|class-string<PriceCast>>
     */
    protected $casts = [
        'id' => 'int',
        'active' => 'bool',
        'fixed_amount' => PriceCast::class,
        'minimum' => PriceCast::class,
        'maximum' => PriceCast::class,
    ];

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setFixedAmount(?Price $fixedAmount): void
    {
        $this->fixed_amount = $fixedAmount;
    }

    public function setQuantityMultiplier(?float $quantityMultiplier): void
    {
        $this->quantity_multiplier = $quantityMultiplier;
    }

    public function setCartAmountPercentage(?float $cartAmountPercentaje): void
    {
        $this->cart_amount_percentage = $cartAmountPercentaje;
    }

    public function setMinimum(?Price $minimum): void
    {
        $this->minimum = $minimum;
    }

    public function setMaximum(?Price $maximum): void
    {
        $this->maximum = $maximum;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFixedAmount(): ?Price
    {
        return $this->fixed_amount;
    }

    public function getQuantityMultiplier(): ?float
    {
        return (float)$this->quantity_multiplier;
    }

    public function getCartAmountPercentage(): ?float
    {
        return $this->cart_amount_percentage;
    }

    public function getMinimum(): ?Price
    {
        return $this->minimum;
    }

    public function getMaximum(): ?Price
    {
        return $this->maximum;
    }

    public function setShippingZoneId(int $shippingZoneId): void
    {
        $this->shipping_zone_id = $shippingZoneId;
    }

    public function getShippingZoneId(): int
    {
        return $this->shipping_zone_id;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function isActive(): bool
    {
        return $this->active;
    }
}
