<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Dtos;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Support\Dtos\Dto;

final class UpdateFixedAmountDto implements Dto
{
    public function __construct(
        public readonly int $id,
        public readonly string $name,
        public readonly int $shipping_zone_id,
        public readonly bool $active,
        public readonly ?Price $fixed_amount,
        public readonly ?float $quantity_multiplier,
        public readonly ?float $cart_amount_percentaje,
        public readonly ?Price $minimum,
        public readonly ?Price $maximum,
    ) {
    }

    /**
     * @throws InvalidPriceException
     */
    public static function map(array $data): Dto
    {
        return new self(
            (int) $data['id'],
            $data['name'],
            (int) $data['shipping_zone_id'],
            self::getActive($data),
            self::getFixedAmount($data),
            self::getQuantityMultiplier($data),
            self::getCartAmountPercentaje($data),
            self::getMinimum($data),
            self::getMaximum($data),
        );
    }


    /**
     * @param array<string, string> $data
     */
    private static function getActive(array $data): bool
    {
        return isset($data['active']) && (bool)$data['active'];
    }

    /**
     * @param array<string, string> $data
     * @throws InvalidPriceException
     */
    private static function getFixedAmount(array $data): ?Price
    {
        return isset($data['fixed_amount']) ? new Price((float) $data['fixed_amount']) : null;
    }

    /**
     * @param array<string, string> $data
     */
    private static function getQuantityMultiplier(array $data): ?float
    {
        return isset($data['quantity_multiplier']) ? (float)$data['quantity_multiplier'] : null;
    }

    /**
     * @param array<string, string> $data
     */
    private static function getCartAmountPercentaje(array $data): ?float
    {
        return isset($data['cart_amount_percentaje']) ? (float)$data['cart_amount_percentaje'] : null;
    }

    /**
     * @param array<string, string> $data
     * @throws InvalidPriceException
     */
    private static function getMinimum(array $data): ?Price
    {
        return isset($data['minimum']) ? new Price((float) $data['minimum']) : null;
    }

    /**
     * @param array<string, string> $data
     * @throws InvalidPriceException
     */
    private static function getMaximum(array $data): ?Price
    {
        return isset($data['maximum']) ? new Price((float) $data['maximum']) : null;
    }
}
