<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Validation;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Validation\ValidationException;

final class FixedAmountValidator
{
    public function __construct(private readonly Factory $validator)
    {
    }

    /**
     * @param array<string, string> $data
     * @throws ValidationException
     */
    public function validateFixedAmountCreation(array $data): void
    {
        $this->validator->validate($data, $this->getFixedAmountCreationValidationFields());
    }

    /**
     * @return array<string, string>
     */
    public function getFixedAmountCreationValidationFields(): array
    {
        return [
            'name' => 'required|string',
            'shipping_zone_id' => 'required|numeric|exists:shipping_zones,id',
            'fixed_amount' => 'required_without_all:quantity_multiplier,cart_amount_percentaje|nullable',
            'quantity_multiplier' => 'required_without_all:fixed_amount,cart_amount_percentaje|numeric|min:0|nullable',
            'cart_amount_percentaje' => 'required_without_all:fixed_amount,quantity_multiplier|numeric|min:0|nullable',
            'minimum' => 'sometimes|nullable',
            'maximum' => 'sometimes|nullable',
            'active' => 'sometimes',
        ];
    }

    /**
     * @return array<string, string>
     */
    public function getFixedAmounUpdateValidationFields(): array
    {
        return $this->getFixedAmountCreationValidationFields() + [
            'id' => 'required|numeric|exists:shipping_methods_fixed_amount,id',
            ];
    }

    /**
     * @param array<string, string> $data
     */
    public function validateFixedAmountUpdate(array $data): void
    {
        $this->validator->validate($data, $this->getFixedAmounUpdateValidationFields());
    }
}
