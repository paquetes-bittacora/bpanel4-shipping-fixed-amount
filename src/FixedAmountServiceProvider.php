<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount;

use Bittacora\Bpanel4\Shipping\FixedAmount\Commands\InstallCommand;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount;
use Bittacora\Bpanel4\Shipping\ShippingFacade;
use Illuminate\Support\ServiceProvider;

final class FixedAmountServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-fixed-amount';

    public function boot(): void
    {
        $this->commands(InstallCommand::class);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        ShippingFacade::registerShippingMethod('Precio fijo', FixedAmount::class);
    }
}
