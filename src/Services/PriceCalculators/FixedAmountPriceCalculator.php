<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Services\PriceCalculators;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount;

final class FixedAmountPriceCalculator implements ShippingMethodPriceCalculator
{
    public function __construct(
        protected readonly ShippingMethod|FixedAmount $shippingMethod,
        protected readonly Cart                       $cart
    )
    {
    }

    public function getOptionText(): string
    {
        return 'Precio fijo';
    }

    public function isEnabled(): bool
    {
        return true;
    }

    /**
     * @throws InvalidPriceException
     */
    public function getShippingCosts(): ?Price
    {
        $prices = [
            $this->calculateFixedAmountPrice(),
            $this->calculateQuantityMultiplierPrice(),
            $this->calculateCartPercentagePrice(),
        ];

        // Aplico el precio más alto de los calculados, porque se ha definido así el módulo
        $shippingCosts = new Price(max($prices));
        $shippingCosts = $this->applyMinimumPrice($shippingCosts);
        $shippingCosts = $this->applyMaximumPrice($shippingCosts);

        return $shippingCosts;
    }

    /**
     * @param array $prices
     * @return array
     */
    private function calculateFixedAmountPrice(): ?float
    {
        return $this->shippingMethod->getFixedAmount()?->toFloat();
    }

    /**
     * @param array $prices
     * @return array
     */
    private function calculateQuantityMultiplierPrice(): ?float
    {
        $quantityMultiplier = $this->shippingMethod->getQuantityMultiplier() ?? 0;
        return $quantityMultiplier * $this->cart->getItemCount();
    }

    private function calculateCartPercentagePrice(): ?float
    {
        $cartAmountPercentage = $this->shippingMethod->getCartAmountPercentage() ?? 0;
        return $cartAmountPercentage / 100 * $this->cart->getSubtotalWithTaxes()->toFloat();
    }

    private function applyMinimumPrice(Price $shippingCosts): Price
    {
        if (null !== $this->shippingMethod->getFixedAmount()) {
            return $this->shippingMethod->getFixedAmount();
        }

        $minimum = $this->shippingMethod->getMinimum();
        return null !== $minimum && $shippingCosts < $minimum ? $minimum : $shippingCosts;
    }

    private function applyMaximumPrice(Price $shippingCosts): Price
    {
        if (null !== $this->shippingMethod->getFixedAmount()) {
            return $this->shippingMethod->getFixedAmount();
        }

        $maximum = $this->shippingMethod->getMaximum();
        return null !== $maximum && $shippingCosts > $maximum ? $maximum : $shippingCosts;
    }
}
