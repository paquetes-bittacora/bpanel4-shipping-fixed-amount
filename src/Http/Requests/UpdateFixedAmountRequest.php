<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Http\Requests;

use Bittacora\Bpanel4\Shipping\FixedAmount\Validation\FixedAmountValidator;
use Illuminate\Foundation\Http\FormRequest;

final class UpdateFixedAmountRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, string>
     */
    public function rules(FixedAmountValidator $validator): array
    {
        return $validator->getFixedAmounUpdateValidationFields();
    }
}
