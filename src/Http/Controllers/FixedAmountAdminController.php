<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Shipping\FixedAmount\Actions\CreateFixedAmount;
use Bittacora\Bpanel4\Shipping\FixedAmount\Actions\UpdateFixedAmount;
use Bittacora\Bpanel4\Shipping\FixedAmount\Dtos\CreateFixedAmountDto;
use Bittacora\Bpanel4\Shipping\FixedAmount\Dtos\UpdateFixedAmountDto;
use Bittacora\Bpanel4\Shipping\FixedAmount\Http\Requests\CreateFixedAmountRequest;
use Bittacora\Bpanel4\Shipping\FixedAmount\Http\Requests\UpdateFixedAmountRequest;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\Support\Dtos\Exceptions\InvalidDtoClassException;
use Bittacora\Support\Dtos\RequestDtoBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory;

final class FixedAmountAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
    ) {
    }

    public function create(ShippingZone $zone): View
    {
        $this->authorize('bpanel4-shipping.fixed-amount.bpanel.create');

        return $this->view->make('bpanel4-fixed-amount::bpanel.create', [
            'zone' => $zone,
            'model' => null,
            'action' => null,
        ]);
    }

    /**
     * @throws InvalidDtoClassException
     */
    public function store(CreateFixedAmountRequest $request, CreateFixedAmount $createFixedAmount): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.fixed-amount.bpanel.store');
        $dto = (new RequestDtoBuilder($request, CreateFixedAmountDto::class))->toDto();

        $createFixedAmount->handle($dto);

        return $this->redirector->route('bpanel4-shipping.bpanel.zones.edit', ['zone' => $dto->shipping_zone_id])
            ->with('alert-success', __('bpanel4-fixed-amount::alerts.created'));
    }

    public function edit(FixedAmount $model): View
    {
        $this->authorize('bpanel4-shipping.fixed-amount.bpanel.edit');

        return $this->view->make('bpanel4-fixed-amount::bpanel.edit', [
            'model' => $model,
            'action' => null,
        ]);
    }

    /**
     * @throws InvalidDtoClassException
     */
    public function update(UpdateFixedAmountRequest $request, UpdateFixedAmount $updateFixedAmount): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.fixed-amount.bpanel.update');

        $dto = (new RequestDtoBuilder($request, UpdateFixedAmountDto::class))->toDto();

        $updateFixedAmount->handle($dto);

        return $this->redirector->route('bpanel4-shipping.bpanel.zones.edit', ['zone' => $dto->shipping_zone_id])
            ->with('alert-success', __('bpanel4-fixed-amount::alerts.updated'));
    }

    public function destroy(FixedAmount $model): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.fixed-amount.bpanel.destroy');

        $model->delete();

        return $this->redirector->route('bpanel4-shipping.bpanel.zones.edit', [
            'zone' => $model->getShippingZoneId(),
        ])->with('alert-success', __('bpanel4-fixed-amount::alerts.updated'));
    }
}
