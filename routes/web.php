<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Shipping\FixedAmount\Http\Controllers\FixedAmountAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/envio')->name('bpanel4-shipping.fixed-amount.bpanel.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/metodos-de-envio/FixedAmount/{zone}/crear', [FixedAmountAdminController::class, 'create'])->name('create');
        Route::post('/metodos-de-envio/FixedAmount/{zone}/crear', [FixedAmountAdminController::class, 'store'])->name('store');
        Route::get('/metodos-de-envio/FixedAmount/{model}/editar', [FixedAmountAdminController::class, 'edit'])->name('edit');
        Route::post('/metodos-de-envio/FixedAmount/{model}/editar', [FixedAmountAdminController::class, 'update'])->name('update');
        Route::delete('/metodos-de-envio/FixedAmount/{model}/eliminar', [FixedAmountAdminController::class, 'destroy'])->name('destroy');
    });
