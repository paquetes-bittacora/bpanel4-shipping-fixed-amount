<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('shipping_methods_fixed_amount', static function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shipping_zone_id');
            $table->string('name');
            $table->integer('fixed_amount')->nullable(); // El envío es una cantidad fija
            $table->unsignedDecimal('quantity_multiplier')->nullable(); // El envío se calcula multiplicando
                                                                               // este número por el número de items en
                                                                               // el carrito.
            $table->unsignedDecimal('cart_amount_percentage')->nullable(); // El envío se calcula como un
                                                                                  // porcentaje del subtotal del carrito
            $table->integer('minimum')->nullable(); // Gastos de envío mínimos que se aplicarán
            $table->integer('maximum')->nullable(); // Gastos de envío máximos que se aplicarán
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('shipping_zone_id')->references('id')->on('shipping_zones');
        });
    }

    public function down(): void
    {
        Schema::drop('shipping_methods_fixed_amount');
    }
};
