<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Database\Factories;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Database\Factories\ShippingZoneFactory;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<FixedAmount>
 */
final class FixedAmountFactory extends Factory
{
    /**
     * @var string $model
     */
    protected $model = FixedAmount::class;

    /**
     * @return array<string|int|bool>
     * @throws InvalidPriceException
     */
    public function definition(): array
    {
        return [
            'shipping_zone_id' => (new ShippingZoneFactory())->create()->getId(),
            'name' => $this->faker->name(),
            'fixed_amount' => null,
            'active' => true,
        ];
    }

    public function withFixedPrice(Price $price): self
    {
        return $this->state(function (array $attributes) use ($price) {
            return [
                'fixed_amount' => $price,
            ];
        });
    }

    public function withQuantityMultiplier(float $multiplier): self
    {
        return $this->state(function (array $attributes) use ($multiplier) {
            return ['quantity_multiplier' => $multiplier];
        });
    }

    public function withSubtotalPercentage(float $percentage): self
    {
        return $this->state(function (array $attributes) use ($percentage) {
            return ['cart_amount_percentage' => $percentage];
        });
    }
}
