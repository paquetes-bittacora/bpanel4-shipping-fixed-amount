<?php

declare(strict_types=1);

return [
    'created' => 'Envío con precio fijo creado',
    'updated' => 'Envío con precio fijo actualizado',
];
