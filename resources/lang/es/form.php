<?php

declare(strict_types=1);

return [
    'create-fixed-amount' => 'Crear envío con precio fijo',
    'edit-fixed-amount' => 'Editar envío con precio fijo',
    'name' => 'Nombre',
    'fixed-amount' => 'Precio fijo',
    'quantity-multiplier' => 'Multiplicador de cantidad',
    'cart-amount-percentage' => 'Porcentaje del total del carrito',
    'minimum' => 'Mínimo',
    'maximum' => 'Máximo',
];
