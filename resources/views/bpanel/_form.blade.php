@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-fixed-amount::form.new-product'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ $panelTitle }}</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('bpanel4-fixed-amount::form.name'), 'required'=>true, 'value' => old('name') ?? $model?->getName() ])
            @livewire('form::input-number', [
                'name' => 'fixed_amount',
                'labelText' => __('bpanel4-fixed-amount::form.fixed-amount'),
                'required'=> false,
                'value' => old('fixed-amount') ?? $model?->getFixedAmount()->toFloat() ?? 0,
                'min' => 0,
                'step' => 0.01,
                'fieldWidth' => 7,
                'labelWidth' => 3,
                'helpText' => 'Importe que se aplicará como gastos de envío al carrito'
            ])
            @livewire('form::input-number', [
                'name' => 'quantity_multiplier',
                'labelText' => __('bpanel4-fixed-amount::form.quantity-multiplier'),
                'required'=> false,
                'value' => old('quantity_multiplier') ?? $model?->getQuantityMultiplier(),
                'min' => 0,
                'step' => 0.01,
                'fieldWidth' => 7,
                'labelWidth' => 3,
                'helpText' => 'Los gastos de envío serán el resultado de multiplicar este número por el número de items del carrito'
            ])
            @livewire('form::input-number', [
                'name' => 'cart_amount_percentage',
                'labelText' => __('bpanel4-fixed-amount::form.cart-amount-percentage'),
                'required'=> false,
                'value' => old('cart_amount_percentage') ?? $model?->getCartAmountPercentage(),
                'min' => 0,
                'step' => 0.01,
                'fieldWidth' => 7,
                'labelWidth' => 3,
                'helpText' => 'Los gastos de envío serán el porcentaje indicado del importe total del carrito'
            ])
            @livewire('form::input-number', [
                'name' => 'minimum',
                'labelText' => __('bpanel4-fixed-amount::form.minimum'),
                'required'=> false,
                'value' => old('minimum') ?? $model?->getMinimum(),
                'min' => 0,
                'step' => 0.01,
                'fieldWidth' => 7,
                'labelWidth' => 3,
                'helpText' => 'Gastos de envío mínimos a aplicar. Se ignorará para "Precio fijo"'
            ])
            @livewire('form::input-number', [
                'name' => 'maximum',
                'labelText' => __('bpanel4-fixed-amount::form.maximum'),
                'required'=> false,
                'value' => old('maximum') ?? $model?->getMaximum(),
                'min' => 0,
                'step' => 0.01,
                'fieldWidth' => 7,
                'labelWidth' => 3,
                'helpText' => 'Gastos de envío máximos a aplicar. Se ignorará para "Precio fijo"'
            ])
            @livewire('form::input-checkbox', [
                'name' => 'active',
                'value' => 1,
                'checked' => $model?->isActive() ?? false,
                'labelText' => __('bpanel4-shipping::form.active'), 'bpanelForm' => true
            ])
            <div class="form-row">
                <div class="col-sm-3"></div>
                <div class="col-sm-7 text-muted small">
                    <p>Si configura varias opciones, se aplicará el cálculo que resulte en el mayor importe. Por
                       ejemplo, si se configura un precio fijo de 5€ y un % de precio del carrito del 10%, y el importe
                       total
                       del carrito es de 100€, se aplicarán unos gastos de envío de 10€. Si el importe del carrito
                       fuesen 40€, se
                       aplicarían unos gastos de envío de 5€.</p>
                </div>
            </div>
            <div class="col-12 mt-3 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @if (null !== $model)
                <input type="hidden" name="id" value="{{ $model->getId() }}">
                <input type="hidden" name="shipping_zone_id" value="{{ $model->getShippingZoneId() }}">
            @else
                <input type="hidden" name="shipping_zone_id" value="{{ $zone->getId() }}">
            @endif
        </form>
        @if($errors->any())
            {{ implode('', $errors->all('<div>:message</div>')) }}
        @endif
    </div>

@endsection
