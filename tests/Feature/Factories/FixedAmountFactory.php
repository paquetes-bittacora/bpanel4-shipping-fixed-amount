<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Tests\Feature\Factories;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;

final class FixedAmountFactory
{
    /**
     * @throws InvalidPriceException
     */
    public function getFixedAmount(ShippingZone $shippingZone): FixedAmount
    {
        $fixedAmount = new FixedAmount();
        $fixedAmount->setName('Envío fijo');
        $fixedAmount->setFixedAmount(new Price(10));
        $fixedAmount->setShippingZoneId($shippingZone->getId());
        $fixedAmount->save();

        return $fixedAmount;
    }
}
