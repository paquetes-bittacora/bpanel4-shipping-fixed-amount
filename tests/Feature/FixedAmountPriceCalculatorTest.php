<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Tests\Feature;

use Bittacora\Bpanel4\Orders\Database\Factories\CartFactory;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\FixedAmount\Database\Factories\FixedAmountFactory;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount;
use Bittacora\Bpanel4\Shipping\Services\PriceCalculators\FixedAmountPriceCalculator;
use Bittacora\Bpanel4\Products\Database\Factories\CartProductFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Exception;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use function random_int;

use Tests\TestCase;

final class FixedAmountPriceCalculatorTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws InvalidPriceException
     * @throws Exception
     */
    public function testDevuelveElPrecioFijadoEnElPanelDeControl(): void
    {
        $expectedPrice = new Price(random_int(10, 1000));
        $fixedAmount = (new FixedAmountFactory())->withFixedPrice($expectedPrice)->createOne();

        $priceCalculator = $this->makePriceCalculator($fixedAmount);

        self::assertEquals($expectedPrice, $priceCalculator->getShippingCosts());
    }

    public function testCalculaElPrecioPorMultiplicadorDeCantidad(): void
    {
        /** @var FixedAmount $fixedAmount */
        $fixedAmount = (new FixedAmountFactory())->withQuantityMultiplier(.37)->createOne();

        $cart = (new CartFactory())->hasAttached(
            (new CartProductFactory())->count(10),
            ['quantity' => 1],
            'products',
        )->create();

        $priceCalculator = new FixedAmountPriceCalculator(
            $fixedAmount,
            $cart,
        );

        $result = $priceCalculator->getShippingCosts()->toFloat();
        self::assertEquals(3.7, $result);
    }

    public function testCalculaLosGastosDeEnvioComoPorcentajeDelTotalDelCarrito(): void
    {
        /** @var FixedAmount $fixedAmount */
        $fixedAmount = (new FixedAmountFactory())->withSubtotalPercentage(10)->createOne();

        $cart = (new CartFactory())->hasAttached(
            (new CartProductFactory())->withPrice(10)->count(10),
            ['quantity' => 1],
            'products',
        )->create();

        $priceCalculator = new FixedAmountPriceCalculator($fixedAmount, $cart);

        $result = $priceCalculator->getShippingCosts()->toFloat();
        self::assertEquals(10, $result);
    }

    public function testAplicaElMinimoDeGastosDeEnvio(): void
    {
        /** @var FixedAmount $fixedAmount */
        $fixedAmount = (new FixedAmountFactory())->withSubtotalPercentage(10)->createOne();
        $minimum = new Price(9999999999);
        $fixedAmount->setMinimum($minimum);

        $cart = (new CartFactory())->hasAttached(
            (new CartProductFactory())->withPrice(10)->count(10),
            ['quantity' => 1],
            'products',
        )->create();

        $priceCalculator = new FixedAmountPriceCalculator($fixedAmount, $cart);

        $result = $priceCalculator->getShippingCosts()->toFloat();
        self::assertEquals($minimum->toFloat(), $result);
    }

    public function testAplicaElMaximoDeGastosDeEnvio(): void
    {
        /** @var FixedAmount $fixedAmount */
        $fixedAmount = (new FixedAmountFactory())->withSubtotalPercentage(10)->createOne();
        $maximum = new Price(4);
        $fixedAmount->setMaximum($maximum);

        $cart = (new CartFactory())->hasAttached(
            (new CartProductFactory())->withPrice(10)->count(10),
            ['quantity' => 1],
            'products',
        )->create();

        $priceCalculator = new FixedAmountPriceCalculator($fixedAmount, $cart);

        $result = $priceCalculator->getShippingCosts()->toFloat();
        self::assertEquals($maximum->toFloat(), $result);
    }

    public function testIgnoraElEnvioMinimoSiSeFijaGastoDeEnvioFijo(): void
    {
        $expectedPrice = new Price(random_int(10, 10000));
        $fixedAmount = (new FixedAmountFactory())->withFixedPrice($expectedPrice)->createOne();
        $minimum = new Price(9999999999);
        $fixedAmount->setMinimum($minimum);

        $priceCalculator = $this->makePriceCalculator($fixedAmount);

        self::assertEquals($expectedPrice, $priceCalculator->getShippingCosts());
    }

    public function testIgnoraElEnvioMaximoSiSeFijaGastoDeEnvioFijo(): void
    {
        $expectedPrice = new Price(random_int(10, 10000));
        $fixedAmount = (new FixedAmountFactory())->withFixedPrice($expectedPrice)->createOne();
        $maximum = new Price(1);
        $fixedAmount->setMaximum($maximum);

        $priceCalculator = $this->makePriceCalculator($fixedAmount);

        self::assertEquals($expectedPrice, $priceCalculator->getShippingCosts());
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $fixedAmount
     * @return FixedAmountPriceCalculator|mixed
     */
    private function makePriceCalculator(\Illuminate\Database\Eloquent\Model $fixedAmount): mixed
    {
        $priceCalculator = $this->app->make(
            FixedAmountPriceCalculator::class,
            [
                'shippingMethod' => $fixedAmount,
                'cart' => (new CartFactory())->createOne(),
            ],
        );
        return $priceCalculator;
    }
}
