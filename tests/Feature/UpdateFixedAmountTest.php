<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Tests\Feature;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\FixedAmount\Actions\UpdateFixedAmount;
use Bittacora\Bpanel4\Shipping\FixedAmount\Database\Factories\FixedAmountFactory;
use Bittacora\Bpanel4\Shipping\FixedAmount\Dtos\UpdateFixedAmountDto;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class UpdateFixedAmountTest extends TestCase
{
    use RefreshDatabase;

    private UpdateFixedAmount $updateFixedAmount;

    public function setUp(): void
    {
        parent::setUp();
        $this->updateFixedAmount = $this->app->make(UpdateFixedAmount::class);
        $this->withoutExceptionHandling();
    }

    public function testActualizaUnEnvioConPrecioFijo(): void
    {
        // Arrange
        $fixedAmount = (new FixedAmountFactory())->create();
        $dto = new UpdateFixedAmountDto(
            $fixedAmount->getId(),
            'Nombre actualizado',
            $fixedAmount->getShippingZoneId(),
            false,
            new Price(12.34),
            null,
            null,
            null,
            null,
        );

        // Act
        $this->updateFixedAmount->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_methods_fixed_amount', [
            'name' => 'Nombre actualizado',
            'fixed_amount' => 123400,
        ]);
    }

    public function testGuardaLosPreciosComoIntAlActualizarDesdeMap(): void
    {
        // Arrange
        $fixedAmount = (new FixedAmountFactory())->create();
        $dto = UpdateFixedAmountDto::map([
            'id' => $fixedAmount->getId(),
            'name' => 'Precio fijo',
            'shipping_zone_id' => $fixedAmount->getShippingZoneId(),
            'fixed_amount' => 12.34,
            'minimum' => 5,
            'maximum' => 20,
        ]);

        // Act
        $this->updateFixedAmount->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_methods_fixed_amount', [
            'name' => 'Precio fijo',
            'fixed_amount' => 123400,
            'minimum' => 50000,
            'maximum' => 200000,
        ]);
    }
}
