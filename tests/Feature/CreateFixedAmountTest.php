<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FixedAmount\Tests\Feature;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Database\Factories\ShippingZoneFactory;
use Bittacora\Bpanel4\Shipping\FixedAmount\Actions\CreateFixedAmount;
use Bittacora\Bpanel4\Shipping\FixedAmount\Dtos\CreateFixedAmountDto;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

final class CreateFixedAmountTest extends TestCase
{
    use RefreshDatabase;

    private CreateFixedAmount $createFixedAmount;

    public function setUp(): void
    {
        parent::setUp();
        $this->createFixedAmount = $this->app->make(CreateFixedAmount::class);
        $this->withoutExceptionHandling();
    }

    /**
     * @throws InvalidPriceException
     * @throws ValidationException
     */
    public function testCreaUnMetodoDeEnvioConGastoFijo(): void
    {
        // Arrange
        $shippingZone = (new ShippingZoneFactory())->create();
        $name = 'Envío fijo ' . microtime();
        $dto = new CreateFixedAmountDto(
            $name,
            $shippingZone->getId(),
            true,
            new Price(10),
            null,
            null,
            null,
            null,
        );

        // Act
        $this->createFixedAmount->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_methods_fixed_amount', [
            'name' => $name,
            'fixed_amount' => '100000',
        ]);
    }

    public function testCreaEnvioFijoConMultiplicadorDeCantidad(): void
    {
        // Arrange
        $shippingZone = (new ShippingZoneFactory())->create();
        $name = 'Envío fijo ' . microtime();
        $dto = new CreateFixedAmountDto(
            $name,
            $shippingZone->getId(),
            true,
            null,
            3,
            null,
            null,
            null,
        );

        // Act
        $this->createFixedAmount->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_methods_fixed_amount', [
            'name' => $name,
            'quantity_multiplier' => '3',
        ]);
    }

    public function testCreaEnvioFijoConPorcentajeDelCarrito(): void
    {
        // Arrange
        $shippingZone = (new ShippingZoneFactory())->create();
        $name = 'Envío fijo ' . microtime();
        $dto = new CreateFixedAmountDto(
            $name,
            $shippingZone->getId(),
            true,
            null,
            null,
            10,
            null,
            null,
        );

        // Act
        $this->createFixedAmount->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_methods_fixed_amount', [
            'name' => $name,
            'cart_amount_percentage' => '10',
        ]);
    }

    public function testDebeEstablecerseUnoDeLosMetodosDeCalculo(): void
    {
        // Arrange
        $this->expectException(ValidationException::class);
        $shippingZone = (new ShippingZoneFactory())->create();
        $name = 'Envío fijo ' . microtime();
        $dto = new CreateFixedAmountDto(
            $name,
            $shippingZone->getId(),
            true,
            null,
            null,
            null,
            null,
            null,
        );

        // Act
        $this->createFixedAmount->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_methods_fixed_amount', [
            'name' => $name,
        ]);
    }

    public function testLasCantidadesSeGuardanComoIntAlCrearDesdeMap(): void
    {
        // Arrange
        $shippingZone = (new ShippingZoneFactory())->create();
        $dto = CreateFixedAmountDto::map([
            'name' => 'Precio fijo',
            'shipping_zone_id' => $shippingZone->getId(),
            'fixed_amount' => 12.34,
            'minimum' => 5,
            'maximum' => 20,
            ]);

        // Act
        $this->createFixedAmount->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_methods_fixed_amount', [
            'name' => 'Precio fijo',
            'fixed_amount' => 123400,
            'minimum' => 50000,
            'maximum' => 200000,
        ]);
    }
}
